NVIDIA_ARCH="NVIDIA-Linux-x86_64-550.54.15"

chmod +x ${NVIDIA_ARCH}.run
rm -rf ${NVIDIA_ARCH}
sh ${NVIDIA_ARCH}.run -x --target ${NVIDIA_ARCH}
ln -s /lib /lib64

cd ${NVIDIA_ARCH}
./nvidia-installer --no-kernel-modules -s --no-install-compat32-libs \
--glvnd-egl-config-path=/usr/share/glvnd/egl_vendor.d --accept-license --disable-nouveaur

#nvidia-ctk cdi generate --output=/etc/cdi/nvidia.yaml

#kubectl label node db-worker-1 gpu=nvidia  
#kubectl label node db-worker-1 feature.node.kubernetes.io/gpu=true
#清除
#kubectl label node db-worker-1 gpu- --overwrite
#kubectl label node db-worker-1 feature.node.kubernetes.io/gpu=false --overwrite

#kubectl label node db-worker-1 nvidia.com/gpu.count=4

#nvidia-ctk runtime configure --runtime=containerd --set-as-default
#nvidia-ctk config --in-place --set nvidia-container-runtime.mode=cdi   //none auto

alais cdi='ssh root@192.168.11.101 nvidia-ctk cdi list'
alais check_gpu='lspci|grep -i nvidia;cdi; nvidia-smi'