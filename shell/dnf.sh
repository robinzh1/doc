rpm -ivh --nodeps perl-MIME-Base64-3.16-500.fc39.x86_64.rpm
rpm -i *.rpm

[fedora]
name=Fedora 38 - x86_64 - aliyun
baseurl=http://mirrors.aliyun.com/fedora/releases/38/Everything/x86_64/os/
enabled=1
metadata_expire=7d
gpgcheck=0
proxy=127.0.0.1:8888


[latest]
name=latest
baseurl=http://os.rd.com/repo/latest
enabled=1
gpgcheck=0
proxy=127.0.0.1:8837

VER=latest
cat << EOF > /etc/yum.repos.d/$VER.repo
[$VER]
name=$VER
baseurl=http://os.rd.com/repo/$VER
enabled=1
gpgcheck=0
proxy=127.0.0.1:8837
EOF

/etc/systemd/system/jenkins.service
[Unit]
Description=Jenkins Continuous Integration Server
After=network.target

[Service]
Type=simple
WorkingDirectory=/opt/jenkins
ExecStart=/usr/bin/java -jar /opt/jenkins/jenkins.war --httpPort=8080
Restart=on-failure

[Install]
WantedBy=multi-user.target

/etc/dnf/dnf.conf
proxy=http://127.0.0.1:8888
