#include <iostream>
#include <cmath>
 
#include <iostream>
#include <cmath>

int example() {
    // 1. 求平方根
    double number = 25.0;
    double squareRootResult = sqrt(number);
    std::cout << "Square root of " << number << " is: " << squareRootResult << std::endl;

    // 2. 求幂运算
    double base = 2.0;
    double exponent = 3.0;
    double powerResult = pow(base, exponent);
    std::cout << base << " raised to the power of " << exponent << " is: " << powerResult << std::endl;

    // 3. 计算绝对值
    double value = -10.5;
    double absoluteValue = fabs(value);
    std::cout << "Absolute value of " << value << " is: " << absoluteValue << std::endl;

    // 4. 求正弦值
    double angleInRadians = 1.0; // 角度转弧度
    double sinValue = sin(angleInRadians);
    std::cout << "Sine of " << angleInRadians << " radians is: " << sinValue << std::endl;

    // 5. 求余弦值
    double cosValue = cos(angleInRadians);
    std::cout << "Cosine of " << angleInRadians << " radians is: " << cosValue << std::endl;

    // 6. 求正切值
    double tanValue = tan(angleInRadians);
    std::cout << "Tangent of " << angleInRadians << " radians is: " << tanValue << std::endl;

    // 7. 求自然对数
    double naturalLog = log(number);
    std::cout << "Natural logarithm of " << number << " is: " << naturalLog << std::endl;

    // 8. 求以e为底的指数
    double exponential = exp(number);
    std::cout << "e raised to the power of " << number << " is: " << exponential << std::endl;

    return 0;
}


double function(double x) {
    return sin(x);
}
 
double numericalIntegration(double a, double b, int n) {
    double h = (b - a) / n;
    double sum = 0.0;
 
    for (int i = 0; i < n; ++i) {
        double x = a + (i + 0.5) * h;
        sum += function(x);
    }
 
    return sum * h;
}
 
int main() {
    double result = numericalIntegration(0.0, 1.0, 1000);
    std::cout << "Numerical integration result: " << result << std::endl;
 
    return 0;
}
