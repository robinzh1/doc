#!/bin/bash
HOSTS=("192.168.11.100" "192.168.11.101" "192.168.11.102")


for host in "${HOSTS[@]}"; do
    echo "update $host"
    scp ~/.bashrc root@$host:~/.bashrc
    scp ~/aldric/local_log.sh root@$host:~/local_log.sh

    last_line=$(ssh root@$host "tail -n 1 /etc/profile")

    if [[ "$last_line" != "source ~/.bashrc" ]]; then
        ssh root@$host "echo 'source ~/.bashrc' >> /etc/profile"
        echo "Appended 'source ~/.bashrc' to /etc/profile on $host"
    else
        echo "'source ~/.bashrc' already exists in /etc/profile on $host"
    fi
done
