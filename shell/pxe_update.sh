#!/bin/bash
ISO="core-image-full-cmdline.iso"

if [[ $1 =~ ^[0-9]+$ ]]; then
        VER=$1
else
        VER="latest"
fi

URL="http://127.0.0.1/ci/${VER}"

upload_to_pxe()
{
   wget -O - "${URL}/${ISO}" | ssh 192.168.254.3 "bash -c 'cat > /images/${ISO}'"
}

check_pxe()
{
   ssh 192.168.25.3 "sha256sum /images/${ISO}"
}


update()
{
    ssh -L 80:192.168.2.7:80 aldric@192.168.3.17 -N &
    SSH_PID=$!
    sleep 1

    echo "Test connection to os.rd.com"
    wget -O os.iso.sha256 ${URL}/text/os.iso.sha256 --timeout=1

    if [ $? -eq 0 ]; then
        echo "connect OK "
        cat os.iso.sha256
        #upload_to_pxe
    else
        echo "Can not connect to CI server, use local latest"
        kill $SSH_PID
        exit 0
    fi

    kill $SSH_PID
    echo "SSH tunnel closed."

}


main()
{
    START_TIME="$(date +%Y-%b-%dT%H:%M:%S)"
    update
    DONE_TIME="$(date +%Y-%b-%dT%H:%M:%S)"
    echo "Start at:${START_TIME} Upload DONE at ${DONE_TIME} , start checking ${VER}"

    check_pxe

    echo "DONE, please check sha256 with http://os.rd.com/ci/${VER}/text/os.iso.sha256"
}

time main
