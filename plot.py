import matplotlib.pyplot as plt
import numpy as np


x = np.linspace(-5, 5, 100)
y1 = x**2
y2 = np.sin(x)

plt.figure(figsize=(8, 6)) 
plt.plot(x, y1, label='y = x^2')  
plt.plot(x, y2, label='y = sin(x)') 

plt.title('Cartesian Coordinate System')
plt.xlabel('X-axis')
plt.ylabel('Y-axis')

plt.legend()

plt.grid(True)

plt.show()
