#include <iostream>
#include <cmath>

// https://github.com/orbingol/gnuplot-iostream Download
#include "gnuplot-iostream.h"

int main() {
    Gnuplot gp;

    std::vector<std::pair<double, double>> parabolaPoints;
    for (double x = -10; x <= 10; x += 0.1) {
        double y = x * x;
        parabolaPoints.push_back(std::make_pair(x, y));
    }
    gp << "plot '-' with lines title 'Parabola'\n";
    gp.send1d(parabolaPoints);

    return 0;
}
