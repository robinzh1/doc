#!/bin/bash
mkdir -p journal


start()
{
    current=$(date +"%m%d_%H%M%S")

    journalctl -f > journal/$(hostname)_${current}_journal.log &
    dmesg -w > journal/$(hostname)_${current}_dmesg.log  &
}

stop()
{
        PID=$(pgrep -f "journalctl -f")
        if [ -n "$PID" ]; then
            kill -9 $PID
            echo "Stopping journal log collection $PID  for $(hostname)."
        fi
        PID=$(pgrep -f "dmesg -w")
        if [ -n "$PID" ]; then
            kill -9 $PID
            echo "Stopping dmesg collection $PID  for $(hostname)."
        fi
}

case "$1" in
    stop)
        stop
        ;;
    *)
        stop
        start
        ;;
esac
