# doc
点这里 [我的博客](https://gitlab.com/aldbin/doc/-/issues)

- [kernel与调度](https://gitlab.com/aldbin/doc/-/issues/1)
- [memory](https://gitlab.com/aldbin/doc/-/issues/2)
- [中断子系统与IO](https://gitlab.com/aldbin/doc/-/issues/3)
- [文件系统](https://gitlab.com/aldbin/doc/-/issues/4)
- [FreeRTOS ThreadX](https://gitlab.com/aldbin/doc/-/issues/5)
- [高性能网络](https://gitlab.com/aldbin/doc/-/issues/6) |[通信网络](https://gitlab.com/aldbin/doc/-/issues/13)
- [HM生态与智驾坐舱](https://gitlab.com/aldbin/doc/-/issues/7)| [智慧家庭](https://gitlab.com/aldbin/doc/-/issues/8)|- [工业控制](https://gitlab.com/aldbin/doc/-/issues/9)
- [架构与设计模式](https://gitlab.com/aldbin/doc/-/issues/10)
- [云原生](https://gitlab.com/aldbin/doc/-/issues/11) | [虚拟化](https://gitlab.com/aldbin/doc/-/issues/16)
- [嵌入式与驱动](https://gitlab.com/aldbin/doc/-/issues/19)
- [Veloren](https://gitlab.com/aldbin/doc/-/issues/18) |[图形GPU DPU](https://gitlab.com/aldbin/doc/-/issues/14) |[深度学习](https://gitlab.com/aldbin/doc/-/issues/17)
- [性能优化](https://gitlab.com/aldbin/doc/-/issues/20) |[c++/Rust 数据结构与算法](https://gitlab.com/aldbin/doc/-/issues/15) |[分布式与并发 锁 可重入与线程安全](https://gitlab.com/aldbin/doc/-/issues/12)

# 寄语
**困惑到洞见**

在技术的深邃森林中探索多年，我经历了从迷茫到清晰，从困惑到洞见的蜕变过程。最初，每一个技术难题都像是密林中的迷雾，让人感到既兴奋又挑战重重。通过不懈的努力——夜以继日地编码、无数次的试错、深入研读技术文档与经典书籍，我逐渐累积起一块块知识的拼图。

终于有一天，当我站在一个特定的技术高峰回望，那些曾经困扰我的概念与难点，仿佛在一瞬间被一束光照亮，所有的碎片瞬间拼接成了完整的画卷。这不仅仅是一种技能上的提升，更是一种思维视角的飞跃。我开始能够从更高的维度审视问题，不再受限于代码的细枝末节，而是能够洞察问题的本质，找到最优雅的解决方案。

这种‘豁然开朗’的体验，就像是长久阴雨后的晴空万里，让人心旷神怡，充满能量。它不仅极大地增强了我的自信心，也赋予了我前所未有的创造力和解决问题的能力。我开始能够预见技术趋势，对新技术的接纳与应用变得更加敏锐与迅速。

在今后的工作中，我将继续深化实践，同时也希望能将这份‘豁然开朗’的视野带给团队，共同推动技术创新，解决更加复杂与有意义的挑战。我相信，技术的每一次精进，都是向着更加辽阔视界的一次迈进，

让我们开启一段新的旅程，共创辉煌

**豁然开朗**
随着时间推移，分散的知识点开始相互连接，形成网络，最终豁然开朗。 不再局限于某一细分领域，而是能够跨学科运用，解决复杂问题，



## python数学编程

##目录
### 第1章　处理数字 1
- 1.1 基本数学运算　1
- 1.2 标签：给数字命名　3
- 1.3 不同类型的数字　4
- 1.3.1 分数的操作　4
- 1.3.2 复数　5
- 1.4 获取用户输入　6
- 1.4.1 处理异常和无效输入　8
- 1.4.2 将分数和复数作为输入　9
- 1.5 编写一个数学计算程序　10
- 1.5.1 计算整数因子　10
- 1.5.2 生成乘法表　12
- 1.5.3 转换测量单位　14
- 1.5.4 求二次方程的根　16
-  编程挑战　19
#1：偶数奇数自动售货机　19
#2：增强型乘法表生成器　19
#3：增强型单位转换器　19
#4：分数计算器　19
#5：为用户设置退出选项　20
### 第　2章 数据可视化　23
- 2.1 了解笛卡儿坐标平面　23
- 2.2 使用列表和元组　24
- 2.3 用matplotlib绘图　26
- 2.3.1 图上的标记　28
- 2.3.2 绘制纽约市的年平均气温　29
- 2.3.3 比较纽约市的月平均气温　31
- 2.3.4 自定义图形　34
- 2.3.5 保存图形　37
- 2.4 用公式绘图　37
- 2.4.1 牛顿万有引力定律　38
- 2.4.2 抛物运动　39
- 编程挑战　44
#1：温度如何变化　45
#2：探索二次函数的可视化　45
#3：增强型抛物轨迹比较程序　46
#4：可视化你的支出　46
#5：探索斐波那契序列与黄金比例　48
### 第3章　数据的统计学特征　50
- 3.1 计算均值　50
- 3.2 计算中位数　52
- 3.3 计算众数并创建频数表　54
- 3.3.1 寻找最常见的元素　54
- 3.3.2 计算众数　55
- 3.3.3 创建频数表　57
- 3.4 测量离散度　59
- 3.4.1 计算一组数字的极差　59
- 3.4.2 计算方差和标准差　60
- 3.5 计算两个数据集之间的相关性　62
- 3.5.1 计算相关系数　63
- 3.5.2 高中成绩和大学入学考试成绩　64
- 3.6 散点图　67
- 3.7 从文件中读取数据　68
- 3.7.1 从文本文件中读取数据　69
- 3.7.2 从CSV文件中读取数据　70
- 编程挑战　73
#1：更好的相关系数计算程序　73
#2：统计计算器　73
#3：用其他CSV数据做实验　73
#4：计算百分位数　74
#5：创建分组频数表　74
### 第4章　用SymPy包解代数和符号数学问题　76
- 4.1 定义符号和符号运算　76
- 4.2 使用表达式　78
- 4.2.1 分解和展开表达式　78
- 4.2.2 使表达式整齐输出　79
- 4.2.3 输出级数　80
- 4.2.4 用值替代符号　81
- 4.2.5 将字符串转换为数学表达式　84
- 4.2.6 表达式乘法　85
- 4.3 解方程　86
- 4.3.1 解二次方程　86
- 4.3.2 用其他变量求解一个变量　87
- 4.3.3 解线性方程组　88
- 4.4 用SymPy包绘图　88
- 4.4.1 绘制用户输入的表达式　91
- 4.4.2 多函数图形绘制　92
- 编程挑战　94
#1：寻找因子　94
#2：图形方程求解器　94
#3：级数求和　94
#4：解单变量不等式　95
### 第5章　集合与概率　98
- 5.1 什么是集合？　98
- 5.1.1 构建集合　99
- 5.1.2 子集、超集与幂集　100
- 5.1.3 集合运算　102
- 5.2 概率　106
- 5.2.1 事件A或事件B发生的概率　108
- 5.2.2 事件A与事件B同时发生的概率　109
- 5.2.3 生成随机数　109
- 5.2.4 非均匀随机数　112
- 编程挑战　114
#1：使用文氏图来可视化集合之间的关系　114
#2：大数定律　117
#3：掷多少次硬币会输光你的钱？　117
#4：洗牌　118
#5：估计一个圆的面积　118
### 第6章　绘制几何图形和分形　120
- 6.1 使用matplotlib的patches绘制几何图形
- 6.1.1 绘制一个圆　122
- 6.1.2 创建动画图形　123
- 6.1.3 抛物轨迹动画演示　125
- 6.2 绘制分形　127
- 6.2.1 平面上点的变换　127
- 6.2.2 绘制Barnsley蕨类植物　131
- 编程挑战　134
#1：在正方形中填充圆形　134
#2：绘制Sierpiński三角　136
#3：探索Hénon函数　137
#4：绘制Mandelbrot集　138
### 第7章　解微积分问题　142
- 7.1 什么是函数？　142
- 7.1.1 函数的定义域和值域　143
- 7.1.2 常用数学函数概述　143
- 7.2 SymPy中的假设　144
- 7.3 计算函数极限　145
- 7.3.1 连续复利　147
- 7.3.2 瞬时变化率　147
- 7.4 函数求导　148
- 7.4.1 求导计算器　149
- 7.4.2 求偏导数　150
- 7.5 高阶导数和最大最小值点　150
- 7.6 用梯度上升法求全局最大值　153
- 7.6.1 梯度上升法的通用程序　156
- 7.6.2 关于初始值的附加说明　157
- 7.6.3 步长和epsilon的角色　158
- 7.7 求函数积分　160
- 7.8 概率密度函数　162
- 编程挑战　164
#1：证明函数在一点处的连续性　165
#2：梯度下降法的实现　165
#3：两条曲线围成的面积　165
#4：计算曲线的长度　166
下一步可以探索的事情　168
欧拉项目　168
Python文档　168
#### 附录A　软件安装　170
升级SymPy　172
安装matplotlib-venn　172
启动Python Shell　172
#### 附录B　Python主题概览　177
- B.1 if __name__ == '__main__'　177
- B.2 列表推导（List Comprehensions）　178
- B.3 字典数据结构　180
- B.4 多个返回值（Multiple Return Values）　181
- B.5 异常处理（Exception Handling）　183
- 指定多个异常类型　183
- else代码块　184
- B.6 在Python中读取文件　185
- 一次性读取所有行　186
- 指定一个文件名作为输入　186
- 读取文件时错误的处理　186
- B.7 代码重用　189


## 用python学数学
《用python学数学》是2021年人民邮电出版社出版的图书，作者是彼得·法雷尔（Peter Farrell）。本书向读者展示如何利用编程来让数学学习变得有意义并且充满乐趣。读者在探索代数学、几何学、三角学、矩阵和元胞自动机等领域的关键数学概念时，将学会在Python语言的帮助下使用代码可视化一系列数学问题的解决方案。

## 目录
### 用turtle模块绘制多边形
### 用列表和循环把烦琐的算术变有趣
### 计算平方根
### 用代数学变换和存储数
- 4.1一次方程的解法公式
- 4.2解更高次的方程
- 4.2.1用quad()函数解二次方程
- 4.2.2用plug()函数解三次方程
- 4.3用作图法解方程
- 4.3.1Processing入门
- 4.3.2制作你自己的作图工具
- 4.3.3绘制方程的图像
- 4.3.4用“猜测检验法”求根
- 4.3.5编写guess()函数
### 用几何学变换形状73
- 5.1画一个圆73
- 5.2用坐标指定位置75
- 5.3变换函数76
- 5.3.1用translate()函数平移对象76
- 5.3.2用rotate()旋转对象79
- 5.3.3画一圈圆80
- 5.3.4画一圈正方形81
- 5.4使对象动画化82
- 5.4.1创建变量t82
- 5.4.2旋转各个正方形83
- 5.4.3用pushMatrix()和popMatrix()保存方位
- 5.4.4使正方形绕中心旋转85
- 5.5制作一个可交互的彩虹网格86
- 5.5.1画出呈网格状排列的对象86
- 5.5.2给对象涂上彩虹色87
- 5.6用三角形画出复杂的图案89
- 5.6.130-60-90三角形91
- 5.6.2画一个等边三角形92
- 5.6.3画多个旋转的三角形94
- 5.6.4给旋转加上相位偏移95
- 5.6.5将图案画完96
### 第6章用三角学制造振荡99
- 6.1用三角学做旋转和振荡101
- 6.2编写画多边形的函数102
- 6.2.1用循环画一个正六边形103
- 6.2.2画一个正三角形105
- 6.3画正弦波106
- 6.3.1圆过留痕109
- 6.3.2使用Python内置的enumerate()函数110
- 6.4编写万花尺程序112
- 6.4.1画小圆113
- 6.4.2旋转小圆113
- 6.5画谐波图116
- 6.5.1编写画谐波图的程序117
- 6.5.2瞬间填好列表119
- 6.5.3两个钟摆比一个强120
### 第7章复数123
- 7.1复数坐标系124
- 7.2将复数相加124
- 7.3将一个复数乘以i125
- 7.4将两个复数相乘126
- 7.5编写magnitude()函数127
- 7.6创建芒德布罗集128
- 7.6.1编写mandelbrot()函数130
- 7.6.2给芒德布罗集上色134
- 7.7创建茹利亚集135
### 第8章将矩阵用于计算机图形和方程组138
- 8.1什么是矩阵138
- 8.2矩阵相加139
- 8.3矩阵相乘140
- 8.4矩阵乘法中的顺序很重要144
- 8.5画2D形状144
- 8.6变换矩阵147
- 8.7转置矩阵149
- 8.8实时旋转矩阵152
- 8.9制作3D形状154
- 8.10制作旋转矩阵155
- 8.11用矩阵解方程组159
- 8.11.1高斯消元法159
- 8.11.2编写gauss()函数161
### 第9章用类构建对象168
- 9.1弹跳球程序170
- 9.1.1让小球动起来171
- 9.1.2让小球从墙上弹回172
- 9.1.3不用类创建多个小球173
- 9.1.4用类创建对象174
- 9.2“羊吃草”程序179
- 9.2.1编写表示小羊的类179
- 9.2.2让小羊四处走动.180
- 9.2.3添加能量属性181
- 9.2.4用类创建草182
- 9.2.5让草被吃掉后变成棕色185
- 9.2.6给每只小羊涂上随机的颜色187
- 9.2.7让小羊繁殖188
- 9.2.8让草再生189
- 9.2.9给予进化优势190
### 第10章用递归制作分形193
- 10.1海岸线的长度194
- 10.1.1何为递归195
- 10.1.2编写factorial()函数195
- 10.1.3“种”一棵分形树196
- 10.2科赫雪花200
- 10.3谢尔宾斯基三角形205
- 10.4正方形分形207
- 10.5龙形曲线211
### 第11章元胞自动机216
- 11.1创建一个元胞自动机217
- 11.1.1编写一个细胞类219
- 11.1.2调整细胞大小221
- 11.1.3让CA生长222
- 11.1.4将细胞放入一个矩阵223
- 11.1.5创建细胞列表224
- 11.2奇怪的Python列表225
- 11.2.1列表切片226
- 11.2.2让你的CA自动生长229
- 11.3玩玩“生命游戏”229
- 11.4初等元胞自动机232
### 第12章用遗传算法解决问题238
- 12.1用遗传算法猜出句子239
- 12.1.1编写makeList()函数239
- 12.1.2测试makeList()函数240
- 12.1.3编写score()函数241
- 12.1.4编写mutate()函数241
- 12.1.5生成随机数242
- 12.2解决旅行商问题244
- 12.2.1使用遗传算法245
- 12.2.2编写calcLength()方法251
- 12.2.3测试calcLength()方法251
- 12.2.4随机路线252
- 12.2.5运用猜句程序的突变思想255
- 12.2.6突变列表中的两个数255
- 12.2.7通过交叉改进路线259

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/robinzh1/doc/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
